import { createContext } from 'react';

const AppContext = createContext({
  // correspondencies: {},
  // email: null,
  // isBlocked: false,
  // phoneNumber: null,
  // uid: null,

  user: null,
  userData: null,
  setContext() {
    // real implementation comes from App.jsx
  },
});

export default AppContext;