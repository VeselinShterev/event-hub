import './App.css';
import { useState } from 'react';
import AppContext from './providers/AppContext';
import { BrowserRouter, Routes, useNavigate } from 'react-router-dom';
import { Link, Route } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';

import Register from './views/Register/Register';
import Header from './components/Test/Header/Header';
import Login from './views/Login/Login';
import { logoutUser } from './services/auth.service';
import { auth } from './config/firebase-config';

// import background from '../src/utils/background.jpg';

function App() {

  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  const [user, loading, error] = useAuthState(auth);



  const logout = () => {
    logoutUser()
      .then(() => {
        setAppState({ user: null, userData: null });
      })
  }

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
        <div className="App">
          <div className='bg'>
            <Header />
            <div style={{ textAlign: 'center' }}>
              {/* <Link to='/'>Home</Link> &nbsp; */}
              {appState.user === null
                ? <>
                  <Link to='/register'>Register</Link> &nbsp;
                  <Link to='/login'>Login</Link> &nbsp;
                </> : null}
              {appState.user !== null
                ? <>
                  <button onClick={logout}>Logout</button>
                </> : null}
              {/* <Link to='/tweets'>Tweets</Link> */}
            </div>
            <div className='Content'>
              <Routes>
                {/* <Route path='/' element={<Home />} /> */}
                <Route path='/register' element={<Register />} />
                <Route path='/login' element={<Login />} />
                {/* <Route path='/tweets' element={<Tweets />} /> */}
              </Routes>
            </div>
            {/* <Footer /> */}
          </div>
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
