import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';

const firebaseConfig = {
    apiKey: "AIzaSyAv2YAA7TXT9ib869T6ccZP4MPfCn7-XqI",
    authDomain: "event-hub-9a2ea.firebaseapp.com",
    projectId: "event-hub-9a2ea",
    storageBucket: "event-hub-9a2ea.appspot.com",
    messagingSenderId: "253828904669",
    appId: "1:253828904669:web:992fc19c25cf323da7cd34",
    measurementId: "G-2BJ44VK1LH",
    databaseURL: "https://event-hub-9a2ea-default-rtdb.europe-west1.firebasedatabase.app/"
  };

export const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);