import './Header.css';

const Header = () => {

  return (
    <div className='Header'>
      <div className='Title'>Not Twitter</div>
      <div className='divider'></div>
      <div className='Profile'>Profile</div>
    </div>
  );
};

export default Header;