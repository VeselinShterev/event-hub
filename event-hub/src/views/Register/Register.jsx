import { useState } from 'react';
import './Register.css';
import { registerUser } from '../../services/auth.service';
import { createUserHandle, getUserByHandle } from '../../services/users.service';

const Register = () => {
  const [form, setForm] = useState({
    email: '',
    password: '',
    handle: '',
  });

  const updateForm = prop => e => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const register = (e) => {
    e.preventDefault();

    if (form.password.length < 6) {
      return alert("Password must be longer than 6 characters")
    }

    getUserByHandle(form.handle)
      .then(snapshot => {
        if (snapshot.exists()) {
          return alert(`User with handle @${form.handle} already exists ;(`)
        }

        return registerUser(form.email, form.password)
          .then(u => {
            createUserHandle(form.handle, u.user.uid, u.user.email)
          })
          .then(alert('New user created'))
          .catch(e => {
            if(e.message.includes('email-already-in-use')){
              alert('Email already in use')
            }
          });
      })
      .catch(console.error);
  }

  return (
    <div className='Register'>
      <div className='Form'>
        <label htmlFor='email'>Email: </label>
        <input type="email" id="email" value={form.email} onChange={updateForm('email')}></input><br />
        <label htmlFor='handle'>Handle: </label>
        @<input type="text" id="handle" value={form.handle} onChange={updateForm('handle')}></input><br />
        <label htmlFor='password'>Password: </label>
        <input type="password" id="password" value={form.password} onChange={updateForm('password')}></input><br /><br />
        <button onClick={register}>Register</button>
      </div>
    </div>
  );
};


export default Register;
