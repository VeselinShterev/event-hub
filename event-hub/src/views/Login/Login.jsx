import { useContext, useState } from 'react';
import './Login.css';
import { loginUser } from '../../services/auth.service';
import AppContext from '../../providers/AppContext';
import { getUserData } from '../../services/users.service';
import { useNavigate } from 'react-router-dom';

const Login = () => {
    const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const {setContext} = useContext(AppContext);
  const navigate = useNavigate();

  const updateForm = prop => e => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const login = (e) => {
    e.preventDefault();

    loginUser(form.email, form.password)
      .then(u => {
        setContext({
          user: u.user,
          userData: null
        })

        return getUserData(u.user.uid)
          .then(snapshot => {
            if(snapshot.exists()){
              setContext({
                user: u.user,
                userData: snapshot.val()[Object.keys(snapshot.val())[0]]
              })
              navigate('/home');
            }
          })
      })
      .catch(console.error);
  }

  return (
    <div className='Login'>
      <div className='Form'>
        <label htmlFor='email'>Email: </label>
        <input type="email" id="email" value={form.email} onChange={updateForm('email')}></input><br />
        <label htmlFor='password'>Password: </label>
        <input type="password" id="password" value={form.password} onChange={updateForm('password')}></input><br /><br />
        <button onClick={login}>Login</button>
      </div>
    </div>
  );
};

export default Login;
